﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK.WindowsPhone.SDK.API.Model
{
    public partial class VKProduct
    {
        public long id { get; set; }

        public long owner_id { get; set; }

        public string title { get; set; }

        public VKPrice price { get; set; }

        public VKProductCategory category { get; set; }

        public string thumb_photo { get; set; }

        public long date { get; set; }

        public string description { get; set; }

        public string deactivated { get; set; }

        public int availability { get; set; }

        public List<VKPhoto> photos;

        public int can_comment { get; set; }

        public int can_repost { get; set; }
        
        public VKLikes likes { get; set; }

        public int views_count { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VK.WindowsPhone.SDK.Util;
namespace VkMarket.Models
{

    public class MarketCategory
    {
        public int id { set; get; }
        public string name { set; get; }
    }
    public class MarketSection
    {
        public int id { set; get; }
        public string name { set; get; }
        public List<MarketCategory> categories { set; get; }
    }




    public sealed class VKCategoriesModel
    {

        private static readonly Object s_lock = new Object();
        private static VKCategoriesModel instance = null;

        private VKCategoriesModel()
        {
        }

        public static VKCategoriesModel Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                Monitor.Enter(s_lock);
                VKCategoriesModel temp = new VKCategoriesModel();
                Interlocked.Exchange(ref instance, temp);
                Monitor.Exit(s_lock);
                return instance;
            }
        }


        public async void Load()
        {
            int count = 1000;
            VKRequest request;
            VKBackendResult<VKList<VKProductCategory>> result;

            request = new VKRequest(new VKRequestParameters(
                  "market.getCategories",
                  "offset", "0",
                  "count", count.ToString()));

            result = await request.DispatchAsync<VKList<VKProductCategory>>();///use tuple

            if (result.ResultCode != VKResultCode.Succeeded)
                return;

            var sect = result.Data.items;
            while (result.Data.count > sect.Count)
            {
                request = new VKRequest(new VKRequestParameters(
                                 "market.getCategories",
                                 "offset", "0",
                                 "count", count.ToString()));

                result = await request.DispatchAsync<VKList<VKProductCategory>>();///use tuple
                if (result.ResultCode == VKResultCode.Succeeded)
                    sect.AddRange(result.Data.items);
            }

            Dictionary<int, MarketSection> sectionsMap = new Dictionary<int, MarketSection>();
            //Dictionary<int, MarketCategory> categoriesMap = new Dictionary<int, MarketCategory>();

            foreach(var cat in sect)
            {
                MarketCategory category = new MarketCategory() { id = cat.id, name = cat.name };
                //categoriesMap.Add(category.id, category);

                MarketSection section;
                if (sectionsMap.ContainsKey(cat.section.id))
                    section = sectionsMap[cat.section.id];
                else
                {
                    section = new MarketSection() { id = cat.section.id, name = cat.section.name, categories = new List<MarketCategory>() };
                    sectionsMap.Add(section.id, section);
                }
                section.categories.Add(category);
            }

            m_sections = sectionsMap.Values.ToList();


        }

        private List<MarketSection> m_sections = null;
        public List<MarketSection> sections {
            get
            {
                return m_sections;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using VkMarket.Controls;
using VkMarket.Utils;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace VkMarket.Pages
{
    public class CommentExt
    {
        public VKComment comment { set; get; }
        public VKUser user { set; get; }
        public VKGroup group { set; get; }

        public string Name
        {
            get
            {
                if (user != null)
                    return user.first_name + ' ' + user.last_name;
                else
                    return group?.name;
            }
        }
        public string Photo
        {
            get
            {
                if (user != null)
                    return user.photo_100;
                else
                    return group?.photo_100;
            }
        }
        public string Text { get { return comment?.plain_text; } }
        public long Date { get { return comment != null ? comment.date : 0; } }
        public List<VKAttachment> Attachments { get { return comment?.attachments; } }
    }

    public sealed class VKProductModel : DependencyObject, INotifyPropertyChanged
    {


        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public VKGroup group { set; get; }
        public VKProduct product { set; get; }
        public Dictionary<long, VKUser> usersDict;
        public Dictionary<long, VKGroup> groupsDict;
        public VKCommentsResponse comments { set; get; }
        public List<CommentExt> comments_ext { set; get; }


        public VKGroup Group { get { return group; } }
        public VKProduct Product { get { return product; } }
        public ObservableCollection<CommentExt> CommentsSource { get; set; }

        public static DependencyProperty IsCommentsLoadingProperty = DependencyProperty.Register("IsCommentsLoading",
                    typeof(bool),
                    typeof(VKProductModel),
                    new PropertyMetadata(false, new PropertyChangedCallback(IsCommentsLoadingChange)));
        public bool IsCommentsLoading
        {
            get { return (bool)GetValue(IsCommentsLoadingProperty); }
            private set
            {
                if (IsCommentsLoading != value)
                {
                    SetValue(IsCommentsLoadingProperty, value);
                    NotifyPropertyChanged("IsCommentsLoading");
                }
            }
        }
        private static void IsCommentsLoadingChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as VKProductModel;
            if (control.IsCommentsLoadingChanged != null)
                control.IsCommentsLoadingChanged(d, e);
        }
        public event EventHandler<DependencyPropertyChangedEventArgs> IsCommentsLoadingChanged;

        public static DependencyProperty CanLoadCommentsProperty = DependencyProperty.Register("CanLoadComments",
                    typeof(bool),
                    typeof(VKProductModel),
                    new PropertyMetadata(false));
        public bool CanLoadComments
        {
            get
            {
                if (comments == null || CommentsSource == null)
                    return false;
                else
                    return comments.count != CommentsSource.Count;
            }
        }
        public async Task loadMoreComments()
        {
            if (product == null || !CanLoadComments)
            {
                return;
            }
            IsCommentsLoading = true;
            var request = new VKRequest(new VKRequestParameters(
                       "market.getComments",
                        "owner_id", (-group?.id).ToString(),
                        "item_id", product?.id.ToString(),
                        "offset", comments?.items?.Count.ToString(),
                        "count", "20",
                        "start_comment_id", comments?.items?.First().id.ToString(),
                        "extended", "1",
                        "fields", "photo_100",
                        "need_likes", "1",
                        "sort", "desc"));

            var result = await request.DispatchAsync<VKCommentsResponse>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                var newComments = result.Data;

                var newUsers = new Dictionary<long, VKUser>(newComments?.profiles?.ToDictionary(user => user.id));
                var newGroups = new Dictionary<long, VKGroup>(newComments?.groups?.ToDictionary(group => group.id));

                foreach (var key in newUsers.Keys)
                    usersDict[key] = newUsers[key];
                foreach (var key in newGroups.Keys)
                    groupsDict[key] = newGroups[key];

                comments_ext = new List<CommentExt>();
                newComments.items.Reverse();
                foreach (var com in newComments.items)
                {
                    comments_ext.Add(new CommentExt()
                    {
                        comment = com,

                    });
                    if (com.from_id > 0 && usersDict.ContainsKey(com.from_id))
                        comments_ext.Last().user = usersDict[com.from_id];
                    else
                        comments_ext.Last().user = null;

                    if (com.from_id < 0 && groupsDict.ContainsKey(-com.from_id))
                        comments_ext.Last().group = groupsDict[-com.from_id];
                    else
                        comments_ext.Last().group = null;
                }

                comments_ext.Reverse();

                if (comments_ext.Count > 0)
                {
                    foreach (var com in comments_ext)

                        CommentsSource.Insert(0, com);
                }

                //foreach (var comment in result.Data.items)
                //    CommentsSource.Add(comment);
                //todo: implement ObserveableCollection for addRange() method
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
            IsCommentsLoading = false;

            NotifyPropertyChanged("CanLoadComments");
        }

        public async Task initialize()
        {
            if (product == null)
            {
                return;
            }
            var request = new VKRequest(new VKRequestParameters(
                       "execute.getProductExtended",
                       "owner_id", Math.Abs(product.owner_id).ToString(),
                       "id", product.id.ToString()));

            var result = await request.DispatchAsync<VKProductModel>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                group = result.Data.group;
                comments = result.Data.comments;
                usersDict = new Dictionary<long, VKUser>(comments.profiles.ToDictionary(user => user.id));
                groupsDict = new Dictionary<long, VKGroup>(comments.groups.ToDictionary(group => group.id));

                comments_ext = new List<CommentExt>();

                foreach (var com in comments.items)
                {
                    comments_ext.Add(new CommentExt()
                    {
                        comment = com,

                    });
                    if (com.from_id > 0 && usersDict.ContainsKey(com.from_id))
                        comments_ext.Last().user = usersDict[com.from_id];
                    else
                        comments_ext.Last().user = null;

                    if (com.from_id < 0 && groupsDict.ContainsKey(-com.from_id))
                        comments_ext.Last().group = groupsDict[-com.from_id];
                    else
                        comments_ext.Last().group = null;
                }

                comments_ext.Reverse();

                if (comments_ext.Count > 0)
                    CommentsSource = new ObservableCollection<CommentExt>(comments_ext);
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
        }

    }
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class ProductPage : Page
    {
        VKProductModel model = null;
        ScrollViewer m_mainScrolViewer = null;
        public ProductPage()
        {
            this.InitializeComponent();
            Loaded += ProductPage_Loaded;
        }

        private void ProductPage_Loaded(object sender, RoutedEventArgs e)
        {
            m_mainScrolViewer = CommentListView.FindChildOfType<ScrollViewer>();
            pageHeader.Focus(FocusState.Keyboard);
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            m_mainScrolViewer = CommentListView.FindChildOfType<ScrollViewer>();

            model = new VKProductModel();

            model.product = e.Parameter as VKProduct;

            DataContext = model;

            await model.initialize();

            DataContext = null;
            DataContext = model;

            PhotoFlipView.ItemsSource = model.Product.photos;

        }

        private async void LikePostButtonCLick(object sender, RoutedEventArgs e)
        {
            var likeButton = sender as Button;
            VKRequest request;
            VKBackendResult<VKLikeResponse> result;
            if (model?.product?.likes.user_likes == 0)
            {
                request = new VKRequest(new VKRequestParameters(
                      "likes.add",
                      "type", "market",
                      "owner_id", model?.product.owner_id.ToString(),
                      "item_id", model?.product.id.ToString()));
            }
            else
            {
                request = new VKRequest(new VKRequestParameters(
                      "likes.delete",
                      "type", "market",
                      "owner_id", model?.product.owner_id.ToString(),
                      "item_id", model?.product.id.ToString()));
            }

            result = await request.DispatchAsync<VKLikeResponse>();///use tuple

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                model.product.likes.user_likes = (model.product.likes.user_likes + 1) % 2;
                model.product.likes.count = result.Data.likes;
                likeButton.DataContext = 0;
                likeButton.DataContext = model?.product.likes;
            }
        }

        private void VirtualizingStackPanel_CleanUpVirtualizedItemEvent(object sender, CleanUpVirtualizedItemEventArgs e)
        {
            if (e.Value == PhotoFlipView.Items.Last())
                e.Cancel = true;
        }

        private void DescriptionTextBlock_VisualStateChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var state = (Controls.PowerTextState)e.NewValue;
            if (state != Controls.PowerTextState.Short)
                return;
            if (m_mainScrolViewer == null)
                return;
            var pos = DescriptionTextBlock.TransformToVisual(DescriptionTextBlock.Parent as StackPanel).TransformPoint(new Point(0, 0));
            if (pos.Y < m_mainScrolViewer.VerticalOffset)
            {
                var period = TimeSpan.FromMilliseconds(100);
                Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        m_mainScrolViewer.ChangeView(null, pos.Y - 8, null, false);

                    });

                }, period);
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            PhotoViewer.Instance.Show(PhotoFlipView.ItemsSource, PhotoFlipView.SelectedIndex);
        }

        private void Header_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var backPage = Frame.BackStack.Last();
            if (backPage.SourcePageType != typeof(ShopDetailPage))
            {
                Frame.Navigate(typeof(ShopDetailPage), model.group);
                return;
            }

            var backMarketId = (backPage.Parameter as VKGroup).id;

            if (backMarketId != model.Group.id)
                Frame.Navigate(typeof(ShopDetailPage), model.group);
            else
            {
                NavigationCacheMode = NavigationCacheMode.Disabled;
                Frame.GoBack();
            }

            //var str = backPage.Parameter == ;
        }

        private void PhotoAttachment_Click(object sender, ItemClickEventArgs e)
        {
            var grid = sender as GridView;
            if (grid != null)
                PhotoViewer.Instance.Show(grid.ItemsSource, grid.SelectedIndex);
        }


        private async void MoreComments_Click(object sender, RoutedEventArgs e)
        {
            var prew_height = m_mainScrolViewer.ScrollableHeight;
            await model.loadMoreComments();


            var period = TimeSpan.FromMilliseconds(50);
            Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    var delta = (m_mainScrolViewer.ScrollableHeight - prew_height);
                    m_mainScrolViewer.ChangeView(null, m_mainScrolViewer.VerticalOffset + delta, null, true);

                });

            }, period);

        }

        private async void Send_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var str = CommentTextBox.Text;
            str = str.Trim();

            if (String.IsNullOrWhiteSpace(str))
                return;

            var request = new VKRequest(new VKRequestParameters(
                       "market.createComment",
                        "owner_id", (-model?.group?.id).ToString(),
                        "item_id", model?.product?.id.ToString(),
                        "message", str,
                        "count", "20"));

            var result = await request.DispatchAsync<long>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                if (model.comments.count == 0)
                {
                    model.comments.items = new List<VKComment>();
                    model.groupsDict = new Dictionary<long, VKGroup>();
                    model.usersDict = new Dictionary<long, VKUser>();
                    model.CommentsSource = new ObservableCollection<CommentExt>();
                }
                model.comments.count++;
                model.comments.items.Add(new VKComment()
                {
                    text = str,
                    from_id = SplitViewPage.Current.viewModel.User.id,
                    id = result.Data,
                    date = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0)).Ticks
                });
                model.usersDict[SplitViewPage.Current.viewModel.User.id] = SplitViewPage.Current.viewModel.User;
                model.CommentsSource.Add(new CommentExt()
                {
                    comment = model.comments.items.Last(),
                    user = SplitViewPage.Current.viewModel.User,
                    group = null
                });

                if (CommentListView.ItemsSource == null)
                    CommentListView.ItemsSource = model.CommentsSource;

                CommentTextBox.Text = "";
                CommentLabel.DataContext = null;
                CommentLabel.DataContext = model;

                var period = TimeSpan.FromMilliseconds(50);
                Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        m_mainScrolViewer.ChangeView(null, m_mainScrolViewer.ScrollableHeight, null, true);
                    });

                }, period);
            }
        }
    }
}

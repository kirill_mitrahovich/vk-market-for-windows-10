﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using Windows.Web.Http;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VkMarket.Controls;
using VK.WindowsPhone.SDK;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VK.WindowsPhone.SDK.Util;
using VK.WindowsPhone.SDK_XAML.Pages;
using Windows.Storage;
using Windows.Graphics.Imaging;
using VkMarket.Utils;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace VkMarket.Pages
{

    public class NavMenuItem : DependencyObject
    {
        public static Dictionary<string, string> Gliphs = new Dictionary<string, string> {
            { "Search","\uE094" },
            { "Shops","\uE10F" },
            { "Popular","\uE1CE" },
            { "Favorite","\uE006" },
        };

        public static readonly DependencyProperty CounterProperty = DependencyProperty.Register(
          "Counter",
          typeof(int),
          typeof(NavMenuItem),
          new PropertyMetadata(0)
        );




        public string Glyph { get; set; }
        public string Label { get; set; }
        public int Counter
        {
            get { return (int)GetValue(CounterProperty); }
            set { SetValue(CounterProperty, value); }
        }
        public Type DestPage { get; set; }
        public object Arguments { get; set; }
    }
    public class NavigationMenuViewModel : INotifyPropertyChanged
    {
        private List<String> m_scope = new List<string> { VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS, VKScope.AUDIO, VKScope.MARKET };
        private VKUser m_user;
        //private VKNotificationCounter m_userCounters { get; set; }

        public VKUser User
        {
            get { return m_user; }
            set
            {
                if (m_user != value)
                {
                    m_user = value;
                    NotifyPropertyChanged("Name");
                    NotifyPropertyChanged("PhotoUrl");
                }

            }
        }

        public int SearchCount { get { return 0; } }
        public int ShopsCount { get { return 0; } }
        public int PopularCount { get { return 0; } }
        public int FavoriteCount { get { return 0; } }
        public int MessagesCount { get { return 0; } }


        public string Name { get { return m_user.first_name + " " + m_user.last_name; } }
        public string PhotoUrl { get { return m_user.photo_100; } }
        public List<NavMenuItem> MenuItems { get; set; }

        public NavigationMenuViewModel()
        {


            NavMenuItem searchMenuItem = new NavMenuItem()
            {
                Glyph = NavMenuItem.Gliphs["Search"],
                Label = "Поиск",
                DestPage = typeof(SearchPage),
            };


            NavMenuItem shopsMenuItem = new NavMenuItem()
            {
                Glyph = NavMenuItem.Gliphs["Shops"],
                Label = "Магазины",
                DestPage = typeof(ShopsPages),
            };
            Binding friendsBinding = new Binding() { Source = this, Path = new PropertyPath("ShopsCount") };
            BindingOperations.SetBinding(shopsMenuItem, NavMenuItem.CounterProperty, friendsBinding);

            NavMenuItem popularMenuItem = new NavMenuItem()
            {
                Glyph = NavMenuItem.Gliphs["Popular"],
                Label = "Популярное",
                DestPage = typeof(MainPage),
            };

            NavMenuItem favoriteMenuItem = new NavMenuItem()
            {
                Glyph = NavMenuItem.Gliphs["Favorite"],
                Label = "Избранное",
                DestPage = typeof(FavoritePage),
            };
            Binding favoriteBinding = new Binding() { Source = this, Path = new PropertyPath("FavoriteCount") };
            BindingOperations.SetBinding(favoriteMenuItem, NavMenuItem.CounterProperty, favoriteBinding);



            MenuItems = new List<NavMenuItem>();
            MenuItems.Add(searchMenuItem);
            MenuItems.Add(shopsMenuItem);
            MenuItems.Add(popularMenuItem);
            MenuItems.Add(favoriteMenuItem);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Update()
        {
        }

    }
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class SplitViewPage : Page
    {
        public static SplitViewPage Current = null;

        public NavigationMenuViewModel viewModel
        {
            get; set;
        }
        private List<String> m_scope = new List<string> { VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS, VKScope.AUDIO, VKScope.MARKET, VKScope.GROUPS };

        public SplitViewPage()
        {
            this.InitializeComponent();



            viewModel = new NavigationMenuViewModel();

            RootSplitView.DataContext = viewModel;
            this.Loaded += async (sender, args) =>
            {
                Current = this;
                this.TogglePaneButton.Focus(FocusState.Programmatic);
                var statusbar = "Windows.UI.ViewManagement.StatusBar";
                if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent(statusbar))
                {
                    //await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ShowAsync();
                    await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().HideAsync();
                }
            };

            this.RootSplitView.RegisterPropertyChangedCallback(
                SplitView.DisplayModeProperty,
                (s, a) =>
                {
                    // Ensure that we update the reported size of the TogglePaneButton when the SplitView's
                    // DisplayMode changes.
                    this.CheckTogglePaneButtonSizeChanged();
                });


            SystemNavigationManager.GetForCurrentView().BackRequested += SystemNavigationManager_BackRequested;





            VKSDK.AccessTokenAccepted += (e, a) =>
            {
                var mes = new MessageDialog("AccessTokenAccepted", "Внимание");
            };
            VKSDK.AccessTokenRenewed += (e, a) =>
            {
                var mes = new MessageDialog("AccessTokenRenewed", "Внимание");
            };
            VKSDK.AccessTokenReceived += (e, a) =>
            {
                var mes = new MessageDialog("AccessTokenReceived", "Внимание");
                VKRequest.Dispatch<List<VKUser>>(
                   new VKRequestParameters(
                       "users.get",
                       "fields", "photo_100"),
                   (res) =>
                   {
                       if (res.ResultCode == VKResultCode.Succeeded)
                       {
                           VKExecute.ExecuteOnUIThread(() =>
                           {
                               viewModel.User = res.Data[0];
                               RootSplitView.DataContext = null;
                               RootSplitView.DataContext = viewModel;
                           });
                       }
                       else
                       {
                           System.Diagnostics.Debug.WriteLine("Error execute API method: " + res?.Error.error_msg);
                       }
                   });
            };

            VKSDK.CaptchaRequest = (VKCaptchaUserRequest captchaUserRequest, Action<VKCaptchaUserResponse> action) =>
            {
                new VKCaptchaRequestUserControl().ShowCaptchaRequest(captchaUserRequest, action);
            };


            VKSDK.Initialize("4585467");
            if (VKSDK.WakeUpSession())
            {
                //get data from local storage and update it
            }
            else
            {
                VKSDK.Authorize(m_scope, false, false);


            }
            VKRequest.Dispatch<List<VKUser>>(
                   new VKRequestParameters(
                       "users.get",
                       "fields", "photo_100"),
                   (res) =>
                   {
                       if (res.ResultCode == VKResultCode.Succeeded)
                       {
                           VKExecute.ExecuteOnUIThread(() =>
                           {
                               viewModel.User = res.Data[0];
                               RootSplitView.DataContext = null;
                               RootSplitView.DataContext = viewModel;
                           });
                       }
                       else
                       {
                           System.Diagnostics.Debug.WriteLine("Error execute API method: " + res?.Error.error_msg);
                       }
                   });
        }


        public Frame AppFrame { get { return this.frame; } }

        /// <summary>
        /// Default keyboard focus movement for any unhandled keyboarding
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AppShell_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            FocusNavigationDirection direction = FocusNavigationDirection.None;
            switch (e.Key)
            {
                case Windows.System.VirtualKey.Left:
                case Windows.System.VirtualKey.GamepadDPadLeft:
                case Windows.System.VirtualKey.GamepadLeftThumbstickLeft:
                case Windows.System.VirtualKey.NavigationLeft:
                    direction = FocusNavigationDirection.Left;
                    break;
                case Windows.System.VirtualKey.Right:
                case Windows.System.VirtualKey.GamepadDPadRight:
                case Windows.System.VirtualKey.GamepadLeftThumbstickRight:
                case Windows.System.VirtualKey.NavigationRight:
                    direction = FocusNavigationDirection.Right;
                    break;

                case Windows.System.VirtualKey.Up:
                case Windows.System.VirtualKey.GamepadDPadUp:
                case Windows.System.VirtualKey.GamepadLeftThumbstickUp:
                case Windows.System.VirtualKey.NavigationUp:
                    direction = FocusNavigationDirection.Up;
                    break;

                case Windows.System.VirtualKey.Down:
                case Windows.System.VirtualKey.GamepadDPadDown:
                case Windows.System.VirtualKey.GamepadLeftThumbstickDown:
                case Windows.System.VirtualKey.NavigationDown:
                    direction = FocusNavigationDirection.Down;
                    break;
            }

            if (direction != FocusNavigationDirection.None)
            {
                var control = FocusManager.FindNextFocusableElement(direction) as Control;
                if (control != null)
                {
                    control.Focus(FocusState.Programmatic);
                    e.Handled = true;
                }
            }
        }

        #region BackRequested Handlers

        private void SystemNavigationManager_BackRequested(object sender, BackRequestedEventArgs e)
        {
            bool handled = e.Handled;
            this.BackRequested(ref handled);
            e.Handled = handled;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            bool ignored = false;
            this.BackRequested(ref ignored);
        }

        private void BackRequested(ref bool handled)
        {
            // Get a hold of the current frame so that we can inspect the app back stack.
            if (this.AppFrame == null)
                return;

            if (PhotoViewerPopup.IsOpen)
            {
                PhotoViewerPopup.IsOpen = false;
                handled = true;
                return;
            }

            // Check to see if this is the top-most page on the app back stack.
            if (this.AppFrame.CanGoBack && !handled)
            {
                // If not, set the event to handled and go back to the previous page in the app.
                handled = true;
                //((Page)AppFrame.Content).NavigationCacheMode = NavigationCacheMode.Disabled;
               
                this.AppFrame.GoBack();
            }
        }

        #endregion

        #region Navigation


        /// <summary>
        /// Ensures the nav menu reflects reality when navigation is triggered outside of
        /// the nav menu buttons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void OnNavigatingToPage(object sender, NavigatingCancelEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("Navigated to {0}:", e.SourcePageType);
            //var backStack = AppFrame.BackStack;
            //foreach (var page in backStack)
            //    System.Diagnostics.Debug.WriteLine(page.SourcePageType.ToString());


            //var forwardStack = AppFrame.ForwardStack;
            //if(forwardStack.Count > 0)
            //    System.Diagnostics.Debug.WriteLine("Forward:");
            //foreach (var page in forwardStack)
            //    System.Diagnostics.Debug.WriteLine(page.SourcePageType.ToString());


            


            if (NavMenuList.ItemsSource != null)
            {
                var items = viewModel.MenuItems.Where(item => item.DestPage == e.SourcePageType);
                int index = -1;
                if (items != null && items.Count() > 0)
                {
                    index = viewModel.MenuItems.IndexOf(items.First());
                }
                NavMenuList.SelectionChanged -= NavMenuList_SelectionChanged;
                NavMenuList.SelectedIndex = index;
                NavMenuList.SelectionChanged += NavMenuList_SelectionChanged;

            }

        }

        private void OnNavigatedToPage(object sender, NavigationEventArgs e)
        {
            // After a successful navigation set keyboard focus to the loaded page
            if (e.Content is Page && e.Content != null)
            {
                var control = (Page)e.Content;
                var rootGrid = control.Content as Grid;
                try
                {
                    //var pageHeader = rootGrid?.Children?.FirstOrDefault(item => item is PageHeader);
                    //if (pageHeader == null)
                    //    TogglePaneButton.Visibility = Visibility.Collapsed;
                    //else
                    //    TogglePaneButton.Visibility = Visibility.Visible;
                }
                catch (Exception exc)
                {
                    var mess = exc.Message;
                }

                control.Loaded += Page_Loaded;
            }

            if (AppFrame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                    AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                    AppViewBackButtonVisibility.Collapsed;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ((Page)sender).Focus(FocusState.Programmatic);
            ((Page)sender).Loaded -= Page_Loaded;
            this.RootSplitView.IsPaneOpen = false;
            this.CheckTogglePaneButtonSizeChanged();
        }

        #endregion

        public Rect TogglePaneButtonRect
        {
            get;
            private set;
        }

        /// <summary>
        /// An event to notify listeners when the hamburger button may occlude other content in the app.
        /// The custom "PageHeader" user control is using this.
        /// </summary>
        public event TypedEventHandler<SplitViewPage, Rect> TogglePaneButtonRectChanged;

        /// <summary>
        /// Callback when the SplitView's Pane is toggled open or close.  When the Pane is not visible
        /// then the floating hamburger may be occluding other content in the app unless it is aware.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePaneButton_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckTogglePaneButtonSizeChanged();
        }

        /// <summary>
        /// Check for the conditions where the navigation pane does not occupy the space under the floating
        /// hamburger button and trigger the event.
        /// </summary>
        private void CheckTogglePaneButtonSizeChanged()
        {
            if (this.RootSplitView.DisplayMode == SplitViewDisplayMode.Inline ||
                this.RootSplitView.DisplayMode == SplitViewDisplayMode.Overlay)
            {
                var transform = this.TogglePaneButton.TransformToVisual(this);
                var rect = transform.TransformBounds(new Rect(0, 0, this.TogglePaneButton.ActualWidth, this.TogglePaneButton.ActualHeight));
                this.TogglePaneButtonRect = rect;
            }
            else
            {
                this.TogglePaneButtonRect = new Rect();
            }

            var handler = this.TogglePaneButtonRectChanged;
            if (handler != null)
            {
                // handler(this, this.TogglePaneButtonRect);
                handler.DynamicInvoke(this, this.TogglePaneButtonRect);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        private void NavMenuList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            viewModel.Update();

            ListView list = sender as ListView;
            if (list == null)
                return;

            NavMenuItem menuItem = list.SelectedItem as NavMenuItem;
            if (menuItem != null && menuItem.DestPage != null && menuItem.DestPage != AppFrame.Content.GetType())
            {
                AppFrame.Navigate(menuItem.DestPage);
            }

        }

        #region PhotoViewer methods

        private void ScrollViewer_ViewChanging(object sender, ScrollViewerViewChangingEventArgs e)
        {
            var scr = sender as ScrollViewer;

            if (scr.DataContext != PhotoFlipView.SelectedItem)
                return;

            var flipScrollViewer = PhotoFlipView.FindChildOfType<ScrollViewer>();
            if (flipScrollViewer == null)
                return;

            if (Math.Abs(scr.ZoomFactor - scr.MinZoomFactor) > (float)0.000001)
            {
                if (flipScrollViewer.HorizontalScrollMode == ScrollMode.Enabled)
                    flipScrollViewer.HorizontalScrollMode = ScrollMode.Disabled;
            }
            else
            {
                if (flipScrollViewer.HorizontalScrollMode == ScrollMode.Disabled)
                    flipScrollViewer.HorizontalScrollMode = ScrollMode.Enabled;
            }
        }

        private void VirtualizingStackPanel_CleanUpVirtualizedItemEvent(object sender, CleanUpVirtualizedItemEventArgs e)
        {
            if (e.Value == PhotoFlipView.Items.Last())
                e.Cancel = true;
        }

        private void Image_Loading(FrameworkElement sender, object e)
        {
            Image image = sender as Image;

            if (image == null)
                return;

            ScrollViewer scrollViewer = (image.Parent as Viewbox)?.Parent as ScrollViewer;
            if (scrollViewer == null)
                return;

            VKPhoto photo = image.DataContext as VKPhoto;
            if (photo == null)
                return;

            float horizontalZoom = (float)ActualWidth / (photo.width);
            float verticalZoom = (float)ActualHeight / (photo.height);

            float minZoomFactor = Math.Min(horizontalZoom, verticalZoom);
            float maxZoomFactor = Math.Max(horizontalZoom, verticalZoom) * 4;

            scrollViewer.MinZoomFactor = minZoomFactor;
            scrollViewer.MaxZoomFactor = maxZoomFactor;
            scrollViewer.ChangeView(null, null, scrollViewer.MinZoomFactor, true);

            var grid = scrollViewer.Parent as Grid;
            if (grid == null)
                return;

            var progressBar = grid.Children[0] as ProgressBar;
            if (progressBar == null)
                return;

            var bitmapImage = image.Source as BitmapImage;
            if (bitmapImage == null)
                return;
            bitmapImage.DownloadProgress += (source, args) =>
            {
                progressBar.Value = args.Progress;
            };
            bitmapImage.ImageOpened += (source, args) =>
            {
                progressBar.Value = 0;
                progressBar.Visibility = Visibility.Collapsed;
            };
        }

        private void Image_Tapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            var period = TimeSpan.FromMilliseconds(100);

            ScrollViewer scrollViewer = (sender as ScrollViewer);
            if (scrollViewer == null)
                return;

            if (Math.Abs(scrollViewer.ZoomFactor - scrollViewer.MinZoomFactor) > (float)0.000001)
            {
                Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        scrollViewer.ChangeView(null, null, scrollViewer.MinZoomFactor, false);
                    });
                }, period);
            }
            else
            {
                Image image = (scrollViewer.Content as Viewbox)?.Child as Image;
                if (image == null)
                    return;

                VKPhoto photo = image.DataContext as VKPhoto;
                if (photo == null)
                    return;

                float horizontalZoom = (float)ActualWidth / (photo.width);
                float verticalZoom = (float)ActualHeight / (photo.height);

                float zoomFactor = Math.Max(horizontalZoom, verticalZoom) * 2;


                var pntImage = e.GetPosition(image as UIElement);


                pntImage.X *= (zoomFactor);
                pntImage.Y *= (zoomFactor);

                var pntScv = e.GetPosition(scrollViewer as UIElement);

                Point offsetPoint = new Point()
                {
                    X = pntImage.X - pntScv.X,
                    Y = pntImage.Y - pntScv.Y
                };

                Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        scrollViewer.ChangeView(offsetPoint.X, offsetPoint.Y, zoomFactor, false);
                    });
                }, period);
            }
        }

        private void PhotoFlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CurrentLabel.Text = (PhotoFlipView.SelectedIndex + 1).ToString();
            TotalCountLabel.Text = PhotoFlipView.Items?.Count.ToString();
        }

        private async void SavePhoto_Click(object sender, RoutedEventArgs e)
        {
            var container = PhotoFlipView.ContainerFromItem(PhotoFlipView.SelectedItem);
            var img = container?.FindChildOfType<Image>();
            var bmp = img?.Source as BitmapImage;

            if (img == null || bmp == null)
                return;


            StorageFolder picsFolder = KnownFolders.SavedPictures;
            StorageFile file = await picsFolder.CreateFileAsync(
                String.Format("vk-marker-{0}.jpg", DateTime.Now.ToUniversalTime().Ticks),
                CreationCollisionOption.GenerateUniqueName);

            HttpClient client = new HttpClient();

            var buffer = await client.GetBufferAsync(bmp.UriSource);
            var stream = await file.OpenAsync(FileAccessMode.ReadWrite);

            using (var outputStream = stream.GetOutputStreamAt(0))
            {
                Windows.Storage.Streams.DataWriter writer = new Windows.Storage.Streams.DataWriter(outputStream);
                writer.WriteBuffer(buffer);
                await writer.StoreAsync();
                await outputStream.FlushAsync();
            }
        }


        #endregion

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            VKSDK.Logout();
            AppFrame.Navigate(typeof(MainPage));
            VKSDK.Authorize(m_scope, false, false);
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            AppFrame.Navigate(typeof(AboutPage));
        }
    }
}

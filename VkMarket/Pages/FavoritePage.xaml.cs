﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.Diagnostics;
using Windows.UI.Popups;
using Windows.UI.Xaml.Navigation;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VK.WindowsPhone.SDK.Util;
using VkMarket.Controls;
using VkMarket.Utils;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace VkMarket.Pages
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class FavoriteModel : DependencyObject, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public static DependencyProperty IsProductsLoadingProperty = DependencyProperty.Register("IsProductsLoading",
                typeof(bool),
                typeof(VKMarketDetailModel),
                new PropertyMetadata(false));
        public bool IsProductsLoading
        {
            set { SetValue(IsProductsLoadingProperty, value); NotifyPropertyChanged("IsProductsLoading"); }
            get { return (bool)GetValue(IsProductsLoadingProperty); }
        }
        public bool canLoadProducts { get { return products?.count != ProductsSource?.Count; } }
        public async void loadMoreProducts()
        {
            IsProductsLoading = true;
            var request = new VKRequest(new VKRequestParameters(
                       "fave.getMarketItems",
                       "extended", "1",
                       "offset", ProductsSource.Count.ToString(),
                       "count", "10"));

            var result = await request.DispatchAsync<VKList<VKProduct>>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                products.items.AddRange(result.Data.items);

                foreach (var prod in result.Data.items)
                    ProductsSource.Add(prod);
                //todo: implement ObserveableCollection for addRange() method
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
            IsProductsLoading = false;
        }

        public async Task initialize()
        {
            var request = new VKRequest(new VKRequestParameters(
                            "fave.getMarketItems",
                            "extended", "1",
                            "offset", "0",
                            "count", "5"));

            var result = await request.DispatchAsync<VKList<VKProduct>>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                products = result.Data;

                if (products != null)
                    ProductsSource = new ObservableCollection<VKProduct>(products.items);
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
        }
        public VKList<VKProduct> products { set; get; }
        public ObservableCollection<VKProduct> ProductsSource { set; get; }
    }

    public sealed partial class FavoritePage : Page
    {
        FavoriteModel model;
        ScrollViewer ProductsScrollViewer;

        public FavoritePage()
        {
            this.InitializeComponent();
            Loaded += FavoritePage_Loaded;
            this.NavigationCacheMode = NavigationCacheMode.Enabled;
        }

        private void FavoritePage_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void AboutScrollViewer_ViewChanging(object sender, ScrollViewerViewChangingEventArgs e)
        {
            if (ProductsScrollViewer.ScrollableHeight - e.NextView.VerticalOffset < 600)
            {
                if (model.canLoadProducts && !model.IsProductsLoading)
                    model.loadMoreProducts();
            }
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //if (e.NavigationMode == NavigationMode.Back)
            //{
            //    ProductsPresenter.SelectedIndex = -1;
            //    return;
            //}
            model = new FavoriteModel();

            DataContext = model;

            progressBar.Visibility = Visibility.Visible;
            progressBar.IsIndeterminate = true;

            await model.initialize();

            DataContext = null;
            DataContext = model;

            progressBar.IsIndeterminate = false;
            progressBar.Visibility = Visibility.Collapsed;

            if (model.products == null || model.products.count == 0)
                EmptyPlaceHolder.Visibility = Visibility.Visible;
        }

        private void ProductsPresenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var product = ProductsPresenter.SelectedItem as VKProduct;

            if (product != null)
                Frame.Navigate(typeof(ProductPage), product);
        }

        private async void RepostButtonClick(object sender, TappedRoutedEventArgs e)
        {
            GlyphToggleButton buttom = sender as GlyphToggleButton;
            Grid uiElement = buttom?.Parent as Grid;
            Object vk_object = uiElement?.DataContext;

            if (buttom == null || uiElement == null || vk_object == null)
                return;

            if (vk_object is VKWallPost)
            {
                var post = vk_object as VKWallPost;
                if (post.reposts.user_reposted == 1) return;
            }

            VKBackendResult<VKRepostsResponse> result = null;
            try
            {
                result = await repost(vk_object, "");
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine("Something wrong in like method: " + exp.Message);
                return;
            }


            if (result.ResultCode != VKResultCode.Succeeded)
                return;

            VKLikes likes;
            if (vk_object is VKWallPost)
            {
                var post = vk_object as VKWallPost;
                post.reposts.user_reposted = 1;
                post.reposts.count = result.Data.reposts_count;
                buttom.DataContext = 0;
                buttom.DataContext = post.reposts;
                likes = post.likes;

            }
            else if (vk_object is VKProduct)
            {
                buttom.Toggled = false;
                likes = (vk_object as VKProduct).likes;
            }
            else
                return;

            likes.user_likes = 1;
            likes.count = result.Data.likes_count;

            GlyphToggleButton likeButtom = uiElement.Children.ElementAt(0) as GlyphToggleButton;
            if (likeButtom != null)
            {
                likeButtom.DataContext = 0;
                likeButtom.DataContext = likes;
            }
        }

        private async void LikeButton_Click(object sender, TappedRoutedEventArgs e)
        {
            GlyphToggleButton likeButtom = sender as GlyphToggleButton;
            VKLikes likes = likeButtom?.DataContext as VKLikes;
            FrameworkElement uiElement = likeButtom?.Parent as FrameworkElement;
            Object vk_object = uiElement?.DataContext;

            if (likeButtom == null || likes == null || uiElement == null || vk_object == null)
                return;

            VKBackendResult<VKLikeResponse> result = null;
            try
            {
                result = await like(vk_object);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine("Something wrong in like method: " + exp.Message);
                return;
            }

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                likes.user_likes = (likes.user_likes + 1) % 2;
                likes.count = result.Data.likes;
                likeButtom.DataContext = 0;
                likeButtom.DataContext = likes;

                if(likes.user_likes == 0)
                {
                    model.products.count--;
                    model.products.items.Remove(vk_object as VKProduct);
                    model.ProductsSource.Remove(vk_object as VKProduct);
                }

                if (model.ProductsSource.Count == 0)
                    EmptyPlaceHolder.Visibility = Visibility.Visible;
            }

        }

        public Task<VKBackendResult<VKLikeResponse>> like(Object obj)
        {
            if (obj == null)
                throw new NullReferenceException();

            String vk_type;

            if (obj is VKWallPost)
                vk_type = "post";
            else if (obj is VKProduct)
                vk_type = "market";
            else if (obj is VKPhoto)
                vk_type = "post";
            else
                throw new Exception("Unsupported type object for likeDislike method!" + obj.GetType().Name);

            Type type = obj.GetType();
            String owner_id = type.GetProperty("owner_id").GetValue(obj)?.ToString();
            String item_id = type.GetProperty("id").GetValue(obj)?.ToString();

            VKRequest request;
            VKLikes likes = type.GetProperty("likes").GetValue(obj) as VKLikes;

            if (String.IsNullOrWhiteSpace(vk_type) ||
                    String.IsNullOrWhiteSpace(owner_id) ||
                    String.IsNullOrWhiteSpace(item_id) ||
                    likes == null)
                throw new Exception("Invalid object or data! " + type.Name);

            if (likes.user_likes == 0)
            {
                request = new VKRequest(new VKRequestParameters(
                      "likes.add",
                      "type", vk_type,
                      "owner_id", owner_id,
                      "item_id", item_id));
            }
            else
            {
                request = new VKRequest(new VKRequestParameters(
                      "likes.delete",
                      "type", vk_type,
                      "owner_id", owner_id,
                      "item_id", item_id));
            }

            return request.DispatchAsync<VKLikeResponse>();///use tuple

        }

        public Task<VKBackendResult<VKRepostsResponse>> repost(Object obj, String message)
        {
            if (obj == null)
                throw new NullReferenceException();

            String vk_type;

            if (obj is VKWallPost)
                vk_type = "wall";
            else if (obj is VKProduct)
                vk_type = "market";
            else if (obj is VKPhoto)
                vk_type = "photo";
            else
                throw new Exception("Unsupported type object for likeDislike method!" + obj.GetType().Name);

            Type type = obj.GetType();
            String owner_id = type.GetProperty("owner_id").GetValue(obj)?.ToString();
            String item_id = type.GetProperty("id").GetValue(obj)?.ToString();
            VKRequest request = new VKRequest(new VKRequestParameters(
                 "wall.repost",
                 "object", vk_type + owner_id + "_" + item_id,
                 "message", message,
                 "ref", "null"));

            return request.DispatchAsync<VKRepostsResponse>();
        }

        private void ProductsPresenter_Loaded(object sender, RoutedEventArgs e)
        {
            if (ProductsScrollViewer == null)
            {
                ProductsScrollViewer = ProductsPresenter.FindChildOfType<ScrollViewer>();
                if (ProductsScrollViewer != null)
                    ProductsScrollViewer.ViewChanging += AboutScrollViewer_ViewChanging;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VkMarket.Controls;
using VK.WindowsPhone.SDK;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VK.WindowsPhone.SDK.Util;
using VK.WindowsPhone.SDK_XAML.Pages;
using Windows.Storage;
using Windows.Graphics.Imaging;
using VkMarket.Utils;
// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace VkMarket.Pages
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class AboutPage : Page
    {
        public VKUser user { set; get; }
        public string AppVersion { set; get; }
        public AboutPage()
        {
            this.InitializeComponent();

            Package package = Package.Current;
            PackageId packageId = package.Id;
            PackageVersion version = packageId.Version;

            AppVersion = string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);

            VersionTextBlock.Text = "ver. " + AppVersion;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            VKRequest.Dispatch<List<VKUser>>(new VKRequestParameters(
                "users.get",
                "user_ids" , "1004079",
                "fields", "photo_200"),
                (res) =>
                {
                    if (res.ResultCode == VKResultCode.Succeeded)
                    {
                        VKExecute.ExecuteOnUIThread(() =>
                        {
                            user = res.Data[0];
                            user.nickname = "Разработчик";
                            user.domain = "mailto:[kirill.mitrahovich@yandex.ru]";
                            user.site = "kirill.mitrahovich@yandex.ru";
                            ContentPanel.DataContext = user;
                        });
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Error execute API method: " + res?.Error.error_msg);
                    }
            });
        }

        private async void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var uri = new Uri(user.vk_url);
            await Windows.System.Launcher.LaunchUriAsync(uri);
        }
    }
}

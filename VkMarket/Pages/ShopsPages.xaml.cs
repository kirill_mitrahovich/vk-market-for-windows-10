﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Popups;
using Windows.UI.Xaml.Navigation;

using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VK.WindowsPhone.SDK.Util;

using VkMarket.Utils;
// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace VkMarket.Pages
{
    public class ShopsSearchViewModel : DependencyObject
    {
        public static DependencyProperty IsEmptyResultProperty = DependencyProperty.Register("IsEmptyResult",
                typeof(bool),
                typeof(ShopsSearchViewModel),
                new PropertyMetadata(false));
        public bool IsEmptyResult
        {
            set { SetValue(IsEmptyResultProperty, value); }
            get { return (bool)GetValue(IsEmptyResultProperty); }
        }

        public static DependencyProperty IsEmptySerchTextProperty = DependencyProperty.Register("IsEmptySerchText",
                typeof(bool),
                typeof(ShopsSearchViewModel),
                new PropertyMetadata(true, IsEmptySerchTextChanged));
        public bool IsEmptySerchText
        {
            set { SetValue(IsEmptySerchTextProperty, value); }
            get { return (bool)GetValue(IsEmptySerchTextProperty); }
        }
        public event EventHandler SerchTextApplied;

        private static void IsEmptySerchTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as ShopsSearchViewModel;
            if (obj.SerchTextApplied != null)
                obj.SerchTextApplied(obj, new PropertyChangedEventArgs("IsEmptySerchTextChanged"));
        }

        public static DependencyProperty IsLoadingProperty = DependencyProperty.Register("IsLoading",
                typeof(bool),
                typeof(ShopsSearchViewModel),
                new PropertyMetadata(false, IsShopsLoadingChanged));
        public bool IsLoading
        {
            set { SetValue(IsLoadingProperty, value); }
            get { return (bool)GetValue(IsLoadingProperty); }
        }
        private static void IsShopsLoadingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("is loading changed");
        }

        public static DependencyProperty GroupsProperty = DependencyProperty.Register("Groups",
                typeof(ObservableCollection<VKGroup>),
                typeof(ShopsSearchViewModel),
                new PropertyMetadata(new ObservableCollection<VKGroup>()));
        public ObservableCollection<VKGroup> Groups
        {
            private set { SetValue(GroupsProperty, value); }
            get { return (ObservableCollection<VKGroup>)GetValue(GroupsProperty); }
        }

        public static DependencyProperty CanLoadShopsProperty = DependencyProperty.Register("CanLoadShops",
                typeof(bool),
                typeof(ShopsSearchViewModel),
                new PropertyMetadata(false));
        public bool CanLoadShops
        {
            private set { SetValue(CanLoadShopsProperty, value); }
            get { return (bool)GetValue(CanLoadShopsProperty); }
        }

        public static DependencyProperty TextProperty = DependencyProperty.Register("Text",
                typeof(string),
                typeof(ShopsSearchViewModel),
                new PropertyMetadata("", new PropertyChangedCallback(TextChanged)));
        public string Text
        {
            set { SetValue(TextProperty, value); }
            get { return (string)GetValue(TextProperty); }
        }
        private static void TextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var model = d as ShopsSearchViewModel;

            model.IsLoading = true;
            model.timer.Stop();
            model.timer.Start();
        }

        public async void loadMoreShops()
        {
            if (groups == null || !CanLoadShops)
            {
                return;
            }

            IsLoading = true;

            var request = new VKRequest(new VKRequestParameters(
                   "execute.searchMarket",
                   "q", Text,
                   "offset", groups.items.Count.ToString(),
                   "count", "20"));

            var result = await request.DispatchAsync<VKList<VKGroup>>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                groups.items.AddRange(result.Data.items);

                for (int i = Groups.Count; i < groups.items.Count; i++)
                {
                    Groups.Add(groups.items[i]);
                }

            }

            CanLoadShops = Groups.Count < groups.count;
            IsLoading = false;
        }
        public async void search()
        {
            if (string.IsNullOrWhiteSpace(Text))
            {
                clear();
                return;
            }

            IsEmptySerchText = false;

            var request = new VKRequest(new VKRequestParameters(
                    "execute.searchMarket",
                    "q", Text,
                    "offset", "0",
                    "count", "20"));

            var result = await request.DispatchAsync<VKList<VKGroup>>();

            if (timer.IsEnabled)
                return;

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                groups = result.Data;
                Groups.Clear();

                if (groups.count > 0 && groups.items != null)
                {
                    for (int i = Groups.Count; i < groups.items.Count; i++)
                        Groups.Add(groups.items[i]);
                }

                CanLoadShops = Groups.Count < groups.count;
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }

            if (groups.count == 0)
                IsEmptyResult = true;
            else
                IsEmptyResult = false;

            IsLoading = false;
        }

        public void clear()
        {
            Text = "";
            if (groups != null)
            {
                groups.count = 0;
                groups.items = null;
            }
            Groups?.Clear();
            CanLoadShops = false;
            IsLoading = false;
            IsEmptyResult = false;
            IsEmptySerchText = true;
        }

        private VKList<VKGroup> groups;
        private DispatcherTimer timer;

        public ShopsSearchViewModel()
        {

            timer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(1000) };
            timer.Tick += (object sender, object e) =>
            {
                timer.Stop();
                search();

            };
        }


    }
    public class ShopsViewModel : DependencyObject
    {
        public static DependencyProperty IsShopsLoadingProperty = DependencyProperty.Register("IsShopsLoading",
                typeof(bool),
                typeof(ShopsViewModel),
                new PropertyMetadata(false));
        public bool IsShopsLoading
        {
            set { SetValue(IsShopsLoadingProperty, value); }
            get { return (bool)GetValue(IsShopsLoadingProperty); }
        }

        public static DependencyProperty GroupsProperty = DependencyProperty.Register("Groups",
                typeof(ObservableCollection<VKGroup>),
                typeof(ShopsViewModel),
                new PropertyMetadata(new ObservableCollection<VKGroup>()));
        public ObservableCollection<VKGroup> Groups
        {
            private set { SetValue(GroupsProperty, value); }
            get
            {
                var value = (ObservableCollection<VKGroup>)GetValue(GroupsProperty);
                return value;
            }
        }

        public static DependencyProperty CanLoadShopsProperty = DependencyProperty.Register("CanLoadShops",
                typeof(bool),
                typeof(ShopsViewModel),
                new PropertyMetadata(false));
        public bool CanLoadShops
        {
            private set { SetValue(CanLoadShopsProperty, value); }
            get { return (bool)GetValue(CanLoadShopsProperty); }
        }

        public async void loadMoreShops()
        {
            if (groups == null || !CanLoadShops)
            {
                return;
            }

            IsShopsLoading = true;

            var request = new VKRequest(new VKRequestParameters(
                   "groups.get",
                   "extended", "1",
                   "fields", "market,city,members_count",
                   "offset", groups.items.Count.ToString(),
                   "count", "15"));

            var result = await request.DispatchAsync<VKList<VKGroup>>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                groups.items.AddRange(result.Data.items);

                for (int i = Groups.Count; i < groups.items.Count; i++)
                {
                    Groups.Add(groups.items[i]);
                }

            }

            CanLoadShops = Groups.Count < groups.count;
            IsShopsLoading = false;
        }
        public async Task initialize()
        {
            IsShopsLoading = true;
            var request = new VKRequest(new VKRequestParameters(
                   "groups.get",
                   "extended", "1",
                   "fields", "market,city,members_count",
                   "offset", "0",
                   "count", "15"));

            var result = await request.DispatchAsync<VKList<VKGroup>>();


            if (result.ResultCode == VKResultCode.Succeeded)
            {
                groups = result.Data;

                for (int i = Groups.Count; i < groups.items.Count; i++)
                {
                    Groups.Add(groups.items[i]);
                }

                CanLoadShops = Groups.Count < groups.count;
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }


            IsShopsLoading = false;
        }
        private VKList<VKGroup> groups;


    }
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class ShopsPages : Page
    {
        public ShopsViewModel Model { set; get; }
        public ShopsSearchViewModel SearchModel { set; get; }

        public bool IsOpenSearchPopup { set; get; }
        public ShopsPages()
        {
            this.InitializeComponent();


            this.NavigationCacheMode = NavigationCacheMode.Enabled;

            System.Diagnostics.Debug.WriteLine("********** Constructor ShopsPages");
        }

        ~ShopsPages()
        {
            System.Diagnostics.Debug.WriteLine("********** Destructor ShopsPages");
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (Model != null)
            {
                if (IsOpenSearchPopup == true)
                    SearchPopup.IsOpen = true;

                await Model.initialize();

                if (Model.Groups.Count == 0)
                    EmptyPlaceHolder.Visibility = Visibility.Visible;

                return;
            }

            Model = new ShopsViewModel();
            SearchModel = new ShopsSearchViewModel();
            SearchModel.SerchTextApplied += SearchModel_SerchTextApplied;


            await Model.initialize();

            if (Model.Groups.Count == 0)
                EmptyPlaceHolder.Visibility = Visibility.Visible;




        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            if (e.NavigationMode == NavigationMode.Back && SearchPopup.IsOpen == true)
            {
                SearchModel.clear();
                IsOpenSearchPopup = SearchPopup.IsOpen = false;
                e.Cancel = true;
            }
        }
        private void SearchModel_SerchTextApplied(object sender, EventArgs e)
        {
            if (SearchModel.IsEmptySerchText)
                VisualStateManager.GoToState(this, "SearchEmpty", true);
            else
                VisualStateManager.GoToState(this, "SearchResult", true);
        }

        private void ShopsListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var group = e.ClickedItem as VKGroup;
            if (group != null)
                Frame.Navigate(typeof(ShopDetailPage), group);
        }

        private void ShopsListView_Loaded(object sender, RoutedEventArgs e)
        {
            var scv = ShopsListView.FindChildOfType<ScrollViewer>();

            if (scv == null)
                return;

            scv.ViewChanging += (object s, ScrollViewerViewChangingEventArgs args) =>
            {
                if (scv.ScrollableHeight - args.NextView.VerticalOffset < 600)
                {
                    if (Model.CanLoadShops && !Model.IsShopsLoading)
                        Model.loadMoreShops();
                }
            };

        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            IsOpenSearchPopup = SearchPopup.IsOpen = true;

            if (SearchModel.IsEmptySerchText)
                VisualStateManager.GoToState(this, "SearchEmpty", true);
            else
                VisualStateManager.GoToState(this, "SearchResult", true);

            var period = TimeSpan.FromMilliseconds(100);

            Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    searchTextBox.Focus(FocusState.Keyboard);
                });
            }, period);

        }

        private void SearchListView_Loaded(object sender, RoutedEventArgs e)
        {
            var scv = SearchListView.FindChildOfType<ScrollViewer>();

            if (scv == null)
                return;

            scv.ViewChanging += (object s, ScrollViewerViewChangingEventArgs args) =>
            {

                SearchListView.Focus(FocusState.Programmatic);

                if (scv.ScrollableHeight - args.NextView.VerticalOffset < 600)
                {
                    if (SearchModel.CanLoadShops && !SearchModel.IsLoading)
                        SearchModel.loadMoreShops();
                }
            };
        }


        private void Item_Holding(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            MenuFlyout menu = FlyoutBase.GetAttachedFlyout(senderElement) as MenuFlyout;

            menu.Items.Clear();

            if (senderElement.DataContext is VKGroup)
            {
            }
                menu.ShowAt(senderElement);
        }
    }
}

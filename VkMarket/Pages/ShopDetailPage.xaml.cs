﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.Diagnostics;
using Windows.UI.Popups;
using Windows.UI.Xaml.Navigation;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VK.WindowsPhone.SDK.Util;
using VkMarket.Controls;
using VkMarket.Utils;
using Windows.ApplicationModel.DataTransfer;
// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace VkMarket.Pages
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>

    public sealed class VKContactExt
    {
        public VKUser User { set; get; }
        public VKContact ContactInfo { set; get; }

    }

    public sealed class ProductSortModel
    {
        public VKMarketSortType Sort { set; get; }
        public int Rev { set; get; }
        public ProductSortModel() { Sort = VKMarketSortType.User; Rev = 1; }
    }

    public sealed class VKMarketDetailModel : DependencyObject
    {

        public VKGroup group { set; get; }
        public VKGroup Group { get { return group; } }

        public List<VKUser> contacts { set; get; }
        public ObservableCollection<VKContactExt> GroupContacts { set; get; }

        public VKList<VKWallPost> posts { set; get; }
        public ObservableCollection<VKWallPost> PostsSource { set; get; }

        public VKList<VKProduct> products { set; get; }
        public ObservableCollection<VKProduct> ProductsSource { set; get; }

        public VKList<VKMarketAlbum> albums { set; get; }
        public ObservableCollection<VKMarketAlbum> AlbumsSource { set; get; }


        public static DependencyProperty IsPostLoadingProperty = DependencyProperty.Register("IsPostLoading",
                typeof(bool),
                typeof(VKMarketDetailModel),
                new PropertyMetadata(false));
        public bool IsPostLoading
        {
            set { SetValue(IsPostLoadingProperty, value); }
            get { return (bool)GetValue(IsPostLoadingProperty); }
        }
        public bool canLoadPost { get { return posts?.count != PostsSource?.Count; } }
        public async void loadMorePosts()
        {
            if (group == null || !canLoadPost)
            {
                return;
            }
            IsPostLoading = true;
            var request = new VKRequest(new VKRequestParameters(
                       "wall.get",
                       "owner_id", "-" + group.id.ToString(),
                       "extended", "0",
                       "filter", "owner",
                       "offset", PostsSource.Count.ToString(),
                       "count", "15"));

            var result = await request.DispatchAsync<VKList<VKWallPost>>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                posts.items.AddRange(result.Data.items);

                foreach (var post in result.Data.items)
                    PostsSource.Add(post);
                //todo: implement ObserveableCollection for addRange() method
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
            IsPostLoading = false;
        }

        public static DependencyProperty IsProductsLoadingProperty = DependencyProperty.Register("IsProductsLoading",
          typeof(bool),
          typeof(VKMarketDetailModel),
          new PropertyMetadata(false));
        public bool IsProductsLoading
        {
            set { SetValue(IsProductsLoadingProperty, value);}
            get { return (bool)GetValue(IsProductsLoadingProperty); }
        }
        public bool canLoadProducts { get { return products?.count != ProductsSource?.Count; } }
        public async void loadMoreProducts()
        {
            if (group == null || !canLoadProducts)
            {
                return;
            }
            IsProductsLoading = true;
            var request = new VKRequest(new VKRequestParameters(
                       "market.search",
                       "sort", ((int)SortModel.Sort).ToString(),
                       "rev", SortModel.Rev == 0 ? "0" : "1",
                       "owner_id", "-" + group.id.ToString(),
                       "extended", "1",
                       "offset", ProductsSource.Count.ToString(),
                       "count", "15"));

            var result = await request.DispatchAsync<VKList<VKProduct>>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                products.items.AddRange(result.Data.items);

                foreach (var prod in result.Data.items)
                    ProductsSource.Add(prod);
                //todo: implement ObserveableCollection for addRange() method
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
            IsProductsLoading = false;
        }

        public static DependencyProperty SortModelProperty = DependencyProperty.Register("SortModel",
           typeof(ProductSortModel),
           typeof(VKMarketDetailModel),
           new PropertyMetadata(new ProductSortModel()));
        public ProductSortModel SortModel
        {
            get { return (ProductSortModel)GetValue(SortModelProperty); }
            set
            {
                if ((ProductSortModel)GetValue(SortModelProperty) != value)
                {
                    SetValue(SortModelProperty, value);
                    products.items.Clear();
                    ProductsSource.Clear();

                    loadMoreProducts();

                    Debug.WriteLine("FFFFFFFFFFinish");
                    
                }
            }
        }

        public static DependencyProperty SortingTitleProperty = DependencyProperty.Register("SortingTitle",
          typeof(string),
          typeof(VKMarketDetailModel),
          new PropertyMetadata("по умолчанию"));
        public string SortingTitle
        {
            get { return (string)GetValue(SortingTitleProperty); }
            set
            {
                if (SortingTitle != value)
                {
                    SetValue(SortingTitleProperty, value);
                }
            }
        }


        public static DependencyProperty IsAlbumsLoadingProperty = DependencyProperty.Register("IsAlbumsLoading",
          typeof(bool),
          typeof(VKMarketDetailModel),
          new PropertyMetadata(false));
        public bool IsAlbumsLoading
        {
            set { SetValue(IsAlbumsLoadingProperty, value);}
            get { return (bool)GetValue(IsAlbumsLoadingProperty); }
        }
        public bool canLoadAlbums { get { return albums?.count != AlbumsSource?.Count; } }
        public async Task loadMoreAlbums()
        {
            if (group == null || !canLoadAlbums)
            {
                return;
            }
            // todo: заменить на хранимую процедуру, возвращающую дополнительно id товаров из это подборки
            // для визуализации аля fancy

            //API.market.getAlbums({
            //    "owner_id" :  "-" + group[0].id,
            //    "count" : 10});
            IsAlbumsLoading = true;
            var request = new VKRequest(new VKRequestParameters(
                       "market.getAlbums",
                       "owner_id", "-" + group.id.ToString(),
                       "offset", AlbumsSource.Count.ToString(),
                       "count", "15"));

            var result = await request.DispatchAsync<VKList<VKMarketAlbum>>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                albums.items.AddRange(result.Data.items);

                foreach (var alb in result.Data.items)
                    AlbumsSource.Add(alb);
                //todo: implement ObserveableCollection for addRange() method
            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
            IsAlbumsLoading = false;
        }

        public async Task initialize()
        {
            if (group == null)
            {
                return;
            }
            var request = new VKRequest(new VKRequestParameters(
                       "execute.getMarketExtendedInfo",
                       "id", group.id.ToString()));

            var result = await request.DispatchAsync<VKMarketDetailModel>();

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                group = result.Data.group;
                contacts = result.Data.contacts;
                posts = result.Data.posts;
                products = result.Data.products;
                albums = result.Data.albums;

                if(posts != null)
                    PostsSource = new ObservableCollection<VKWallPost>(posts.items);
                if(products != null)
                    ProductsSource = new ObservableCollection<VKProduct>(products.items);
                if (albums != null)
                    AlbumsSource = new ObservableCollection<VKMarketAlbum>(albums.items);

                if(contacts != null && contacts.Count > 0)
                {
                    List<VKContactExt> cont = new List<VKContactExt>();

                    for(int i = 0; i < contacts.Count; i++)
                    {
                        cont.Add(new VKContactExt()
                        {
                            User = contacts[i],
                            ContactInfo = group.contacts[i]
                        });
                    }

                    GroupContacts = new ObservableCollection<VKContactExt>(cont);
                }


            }
            else {
                ;// await (new MessageDialog("Error execute API method\n" + result.Error.error_msg, "Error")).ShowAsync();
            }
        }
    }

    


    public sealed partial class ShopDetailPage : Page
    {
        VKMarketDetailModel model;
        ScrollViewer AboutScrollViewer;
        ScrollViewer ProductsScrollViewer;
        ScrollViewer AlbumsScrollViewer;

        public ShopDetailPage()
        {
            this.InitializeComponent();

            model = new VKMarketDetailModel();

            //this.NavigationCacheMode = NavigationCacheMode.Enabled;

            bool isHardwareButtonsAPIPresent =
            Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.Phone.UI.Input.HardwareButtons");

            if (isHardwareButtonsAPIPresent)
            {
                // add Windows Mobile extenstions for UWP
                Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed; ;
            }

            Loaded += ShopDetailPage_Loaded;

            System.Diagnostics.Debug.WriteLine("********** Constructor ShopDetailPage");
        }
        ~ShopDetailPage()
        {
            System.Diagnostics.Debug.WriteLine("********** Destructor ShopDetailPage");
        }
        private void ShopDetailPage_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            e.Handled = false;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.Back)
            {
                ProductsPresenter.SelectedIndex = -1;
                AlbumsPresenter.SelectedIndex = -1;
                PostsPresenter.SelectedIndex = -1;
            }


            model.group = e.Parameter as VKGroup;

            DataContext = model;

            progressBar.Visibility = Visibility.Visible;
            progressBar.IsIndeterminate = true;

            await model.initialize();

            DataContext = null;
            DataContext = model;

            progressBar.IsIndeterminate = false;
            progressBar.Visibility = Visibility.Collapsed;
        }

        private void ProductsPresenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var product = ProductsPresenter.SelectedItem as VKProduct;

            if (product != null)
                Frame.Navigate(typeof(ProductPage), product);

            //if (product == null)
            //    return;
            //PhotoViewer.Instance.Show(product.photos, 0);
        }

        private void DescriptionTextBlock_VisualStateChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var state = (Controls.PowerTextState)e.NewValue;
            if (state != Controls.PowerTextState.Short)
                return;
            if (AboutScrollViewer == null)
                return;
            var pos = DescriptionTextBlock.TransformToVisual(DescriptionTextBlock.Parent as StackPanel).TransformPoint(new Point(0, 0));
            if (pos.Y < AboutScrollViewer.VerticalOffset)
            {
                var period = TimeSpan.FromMilliseconds(50);
                Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        AboutScrollViewer.ChangeView(null, pos.Y - 8, null, false);

                    });

                }, period);
            }
        }

        bool m_isPivotTabVisible = true;
        private void PivotItemScrolling(object sender, ScrollViewerViewChangingEventArgs e)
        {
            var scv = sender as ScrollViewer;

            if (scv == null)
                return;

            var delta = e.NextView.VerticalOffset - scv.VerticalOffset;
            if (scv.ScrollableHeight - e.NextView.VerticalOffset < 50)
            {
                VisualStateManager.GoToState(PagePivot, "Normal", true);
                VisualStateManager.GoToState(this, "Normal", true);
                m_isPivotTabVisible = true;
            }
            else if (m_isPivotTabVisible && delta > 5)
            {
                VisualStateManager.GoToState(PagePivot, "Hiden", true);
                VisualStateManager.GoToState(this, "Hiden", true);
                m_isPivotTabVisible = false;
            }
            else if (!m_isPivotTabVisible && delta < -5)
            {
                VisualStateManager.GoToState(PagePivot, "Normal", true);
                VisualStateManager.GoToState(this, "Normal", true);
                m_isPivotTabVisible = true;
            }
        }

        private async void RepostButtonClick(object sender, TappedRoutedEventArgs e)
        {
            GlyphToggleButton buttom = sender as GlyphToggleButton;
            Grid uiElement = buttom?.Parent as Grid;
            Object vk_object = uiElement?.DataContext;

            if (buttom == null || uiElement == null || vk_object == null)
                return;

            if (vk_object is VKWallPost)
            {
                var post = vk_object as VKWallPost;
                if (post.reposts.user_reposted == 1) return;
            }

            VKBackendResult<VKRepostsResponse> result = null;
            try
            {
                result = await repost(vk_object, "");
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine("Something wrong in like method: " + exp.Message);
                return;
            }


            if (result.ResultCode != VKResultCode.Succeeded)
                return;

            VKLikes likes;
            if (vk_object is VKWallPost)
            {
                var post = vk_object as VKWallPost;
                post.reposts.user_reposted = 1;
                post.reposts.count = result.Data.reposts_count;
                buttom.DataContext = 0;
                buttom.DataContext = post.reposts;
                likes = post.likes;

            }
            else if (vk_object is VKProduct)
            {
                buttom.Toggled = false;
                likes = (vk_object as VKProduct).likes;
            }
            else
                return;

            likes.user_likes = 1;
            likes.count = result.Data.likes_count;

            GlyphToggleButton likeButtom = uiElement.Children.ElementAt(0) as GlyphToggleButton;
            if (likeButtom != null)
            {
                likeButtom.DataContext = 0;
                likeButtom.DataContext = likes;
            }
        }

        private async void LikeButton_Click(object sender, TappedRoutedEventArgs e)
        {
            GlyphToggleButton likeButtom = sender as GlyphToggleButton;
            VKLikes likes = likeButtom?.DataContext as VKLikes;
            FrameworkElement uiElement = likeButtom?.Parent as FrameworkElement;
            Object vk_object = uiElement?.DataContext;

            if (likeButtom == null || likes == null || uiElement == null || vk_object == null)
                return;

            VKBackendResult<VKLikeResponse> result = null;
            try
            {
                result = await like(vk_object);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine("Something wrong in like method: " + exp.Message);
                return;
            }

            if (result.ResultCode == VKResultCode.Succeeded)
            {
                likes.user_likes = (likes.user_likes + 1) % 2;
                likes.count = result.Data.likes;
                likeButtom.DataContext = 0;
                likeButtom.DataContext = likes;
            }

        }

        public Task<VKBackendResult<VKLikeResponse>> like(Object obj)
        {
            if (obj == null)
                throw new NullReferenceException();

            String vk_type;

            if (obj is VKWallPost)
                vk_type = "post";
            else if (obj is VKProduct)
                vk_type = "market";
            else if (obj is VKPhoto)
                vk_type = "post";
            else
                throw new Exception("Unsupported type object for likeDislike method!" + obj.GetType().Name);

            Type type = obj.GetType();
            String owner_id = type.GetProperty("owner_id").GetValue(obj)?.ToString();
            String item_id = type.GetProperty("id").GetValue(obj)?.ToString();

            VKRequest request;
            VKLikes likes = type.GetProperty("likes").GetValue(obj) as VKLikes;

            if (String.IsNullOrWhiteSpace(vk_type) ||
                    String.IsNullOrWhiteSpace(owner_id) ||
                    String.IsNullOrWhiteSpace(item_id) ||
                    likes == null)
                throw new Exception("Invalid object or data! " + type.Name);

            if (likes.user_likes == 0)
            {
                request = new VKRequest(new VKRequestParameters(
                      "likes.add",
                      "type", vk_type,
                      "owner_id", owner_id,
                      "item_id", item_id));
            }
            else
            {
                request = new VKRequest(new VKRequestParameters(
                      "likes.delete",
                      "type", vk_type,
                      "owner_id", owner_id,
                      "item_id", item_id));
            }

            return request.DispatchAsync<VKLikeResponse>();///use tuple

        }

        public Task<VKBackendResult<VKRepostsResponse>> repost(Object obj, String message)
        {
            if (obj == null)
                throw new NullReferenceException();

            String vk_type;

            if (obj is VKWallPost)
                vk_type = "wall";
            else if (obj is VKProduct)
                vk_type = "market";
            else if (obj is VKPhoto)
                vk_type = "photo";
            else
                throw new Exception("Unsupported type object for likeDislike method!" + obj.GetType().Name);

            Type type = obj.GetType();
            String owner_id = type.GetProperty("owner_id").GetValue(obj)?.ToString();
            String item_id = type.GetProperty("id").GetValue(obj)?.ToString();
            VKRequest request = new VKRequest(new VKRequestParameters(
                 "wall.repost",
                 "object", vk_type + owner_id + "_" + item_id,
                 "message", message,
                 "ref", "null"));

            return request.DispatchAsync<VKRepostsResponse>();
        }

        private void PagePivot_PivotItemLoading(Pivot sender, PivotItemEventArgs args)
        {
            VisualStateManager.GoToState(PagePivot, "Normal", true);
            VisualStateManager.GoToState(this, "Normal", true);
            m_isPivotTabVisible = true;
        }

        private void PagePivot_PivotItemLoaded(Pivot sender, PivotItemEventArgs args)
        {
            if (AboutScrollViewer == null)
            {
                AboutScrollViewer = PostsPresenter.FindChildOfType<ScrollViewer>();
                if (AboutScrollViewer != null)
                {
                    AboutScrollViewer.ViewChanging += PivotItemScrolling;
                    AboutScrollViewer.ViewChanging += (s, e) =>
                    {
                        if (AboutScrollViewer.ScrollableHeight - e.NextView.VerticalOffset < 600)
                        {
                            if (model.canLoadPost && !model.IsPostLoading)
                                model.loadMorePosts();
                        }
                    };

                }
            }

            if (ProductsScrollViewer == null)
            {
                ProductsScrollViewer = ProductsPresenter.FindChildOfType<ScrollViewer>();
                if (ProductsScrollViewer != null)
                {
                    ProductsScrollViewer.ViewChanging += PivotItemScrolling;
                    ProductsScrollViewer.ViewChanging += (s, e) =>
                    {
                        if (ProductsScrollViewer.ScrollableHeight - e.NextView.VerticalOffset < 600)
                        {
                            if (model.canLoadProducts && !model.IsProductsLoading)
                                model.loadMoreProducts();
                        }
                    };
                }
            }

            if (AlbumsScrollViewer == null)
            {
                AlbumsScrollViewer = AlbumsPresenter.FindChildOfType<ScrollViewer>();
                if (AlbumsScrollViewer != null)
                {
                    AlbumsScrollViewer.ViewChanging += PivotItemScrolling;
                    AlbumsScrollViewer.ViewChanging += async (s, e) =>
                    {
                        if (AlbumsScrollViewer.ScrollableHeight - e.NextView.VerticalOffset < 600)
                        {
                            if (model.canLoadAlbums && !model.IsAlbumsLoading)
                                await model.loadMoreAlbums();
                        }
                    };
                }
            }
        }

        private void MenuFlyoutItem_Click(object sender, RoutedEventArgs e)
        {
            ToggleMenuFlyoutItem item = sender as ToggleMenuFlyoutItem;
            if (item == null || !item.IsChecked)
            {
                item.IsChecked = true;
                return;
            }


            switch (item.Tag.ToString())
            {
                case "default":
                    model.SortModel = new ProductSortModel() { Sort = VKMarketSortType.User, Rev = 1 };
                    break;
                case "popular":
                    model.SortModel = new ProductSortModel() { Sort = VKMarketSortType.Popular, Rev = 1 };
                    break;
                case "date":
                    model.SortModel = new ProductSortModel() { Sort = VKMarketSortType.Date, Rev = 1 };
                    break;
                case "price_tohigh":
                    model.SortModel = new ProductSortModel() { Sort = VKMarketSortType.Price, Rev = 0 };
                    break;
                case "price_tolow":
                    model.SortModel = new ProductSortModel() { Sort = VKMarketSortType.Price, Rev = 1 };
                    break;
                default:
                    model.SortModel = new ProductSortModel() { Sort = VKMarketSortType.User, Rev = 0 };
                    break;
            }

            model.SortingTitle = item.Text;

            foreach (ToggleMenuFlyoutItem child in SortMenuFlyout.Items)
            {
                if (child == item)
                    continue;

                child.IsChecked = false;
            }

        }

        private void SortPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var period = TimeSpan.FromMilliseconds(50);
            Windows.System.Threading.ThreadPoolTimer.CreateTimer(async (source) =>
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    ProductsScrollViewer?.ChangeView(null, 0, null, false);

                });

            }, period);

        }

        private void PhotoAttachment_Click(object sender, ItemClickEventArgs e)
        {
            GridView view = sender as GridView;
            VKPhoto photo = e.ClickedItem as VKPhoto;

            if (view == null || photo == null)
                return;

            PhotoViewer.Instance.Show(view.ItemsSource, view.Items.IndexOf(photo));
        }

        private async void GroupAction_Click(object sender, RoutedEventArgs e)
        {
            MenuFlyoutItem item = sender as MenuFlyoutItem;
            if (item == null)
                return;


            switch (item.Tag.ToString())
            {
                case "unsubscribe":
                    VKRequest request = new VKRequest(new VKRequestParameters(
                              "groups.leave",
                              "group_id", model.group.id.ToString()));

                    var result = await request.DispatchAsync<int>();
                    if(result.ResultCode == VKResultCode.Succeeded)
                    {
                        model.Group.is_member = 0;
                        ActionButton.DataContext = null;
                        ActionButton.DataContext = model;
                    }


                    break;
                case "browser":
                    var uri = new Uri(model.group.vk_url);
                    await Windows.System.Launcher.LaunchUriAsync(uri);
                    break;
                case "clipboard":
                    var dataPackage = new DataPackage { RequestedOperation = DataPackageOperation.Copy };
                    dataPackage.SetText(model.group.vk_url);
                    Clipboard.SetContent(dataPackage);

                    break;
            }

        }

        private async void Subscribe_Click(object sender, RoutedEventArgs e)
        {
            VKRequest request = new VKRequest(new VKRequestParameters(
                              "groups.join",
                              "group_id", model.group.id.ToString()));

            var result = await request.DispatchAsync<int>();
            if (result.ResultCode == VKResultCode.Succeeded)
            {
                model.Group.is_member = 1;
                ActionButton.DataContext = null;
                ActionButton.DataContext = model;
            }


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace VkMarket.Converters
{
    class MembersCountConverters : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is int)
            {
                int count = (int)value % 10;

                if (count == 1)
                    return value.ToString() + " участник";
                else if (count > 1 && count <= 4)
                    return value.ToString() + " участника";
                else
                    return value.ToString() + " участников";

            }

            return value.ToString() + " участников";

        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}

﻿using System;
using System.Globalization;
using Windows.UI.Xaml.Data;

namespace VkMarket.Converters
{
    class UtcTimeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType,
            object parameter, string language)
        {
            if (value is long)
            {
                long utc_time = (long)value;
                DateTime dtime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(utc_time);

                var format = parameter as String;
                if (format != null)
                    return dtime.Date.ToString(format, CultureInfo.CurrentUICulture);
                else
                    return dtime.Date.ToString("D", CultureInfo.CurrentUICulture);
            }
            else
                return null;

        }

        // No need to implement converting back on a one-way binding 
        public object ConvertBack(object value, Type targetType,
            object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}

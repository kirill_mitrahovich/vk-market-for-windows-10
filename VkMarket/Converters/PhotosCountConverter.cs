﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace VkMarket.Converters
{
    class PhotosCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is int)
            {
                int count = (int)value;
                if(count == 1)
                    return count.ToString() + " фотография";
                else if (count > 1 && count <= 4)
                    return value.ToString() + " фотографии";
                else 
                    return count.ToString() + " фотографий";

            }

            return value.ToString() + "фотографий";

        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}

﻿using System;
using Windows.UI.Xaml.Data;
using VK.WindowsPhone.SDK.API.Model;

namespace VkMarket.Converters
{
    //bingmaps:?cp=40.726966~-74.006076&amp;lvl=10
    class GeoToBingLinkConverter : IValueConverter
    {

        public object Convert(object value, Type targetType,
            object parameter, string language)
        {
            VKGeo geoObject = value as VKGeo;
            if (geoObject == null || String.IsNullOrWhiteSpace(geoObject.coordinates))
                return null;

            var coords = geoObject.coordinates.Split(' ');
            if (coords.Length != 2)
                return null;

            var latitude = coords[0];//широтаж
            var longitude = coords[1];//долгота

            return new Uri(String.Format("bingmaps:?collection=point.{0}_{1}_{2}&lvl=16", latitude, longitude, geoObject.place.title.Replace(" ", "%20")));
        }

        // No need to implement converting back on a one-way binding 
        public object ConvertBack(object value, Type targetType,
            object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml;

namespace VkMarket.Converters
{
    class ObjectToVisibleConverter : IValueConverter
    {

        public object Convert(object value, Type targetType,
            object parameter, string language)
        {
            if (value is bool)
            {
                if ((bool)value)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }

            if ( value is String)
            {
                if (!String.IsNullOrWhiteSpace(value as String))
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }

            if (value is int)
            {
                if (((int)value) != 0)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }

            if (value is double)
            {
                if (Math.Equals((double)value,0) )
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;
            }

            if (value is long)
            {
                if ((long)value == 0)
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;
            }

            if (value != null)
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
           
        }

        // No need to implement converting back on a one-way binding 
        public object ConvertBack(object value, Type targetType,
            object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace VkMarket.Converters
{
    class CountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is int) || !(parameter is string))
                return null;

            var type = parameter as string;
            int count = (int)value;

            switch (type)
            {
                case "product":
                    return product(count);
                case "photo":
                    return photo(count);
                case "member":
                    return member(count);
                case "view":
                    return view(count);
                case "comment":
                    return comment(count);
                default:
                    return item(count);

            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }

        public string product(int count)
        {
            int rounded = count % 10;
            if (count == 0)
                return "нет товаров";
            else if (rounded == 1)
                return count.ToString() + " товар";
            else if (rounded > 1 && rounded <= 4)
                return count.ToString() + " товара";
            else
                return count.ToString() + " товаров";
        }
        public string photo(int count)
        {
            int rounded = count % 10;
            if (count == 0)
                return "нет фотографий";
            else if (rounded == 1)
                return count.ToString() + " фотография";
            else if (rounded > 1 && rounded <= 4)
                return count.ToString() + " фотографии";
            else
                return count.ToString() + " фотографий";
        }
        public string member(int count)
        {
            int rounded = count % 10;
            if (count == 0)
                return "нет участников";
            else if (rounded == 1)
                return count.ToString() + " участник";
            else if (rounded > 1 && rounded <= 4)
                return count.ToString() + " участника";
            else
                return count.ToString() + " участников";
        }
        public string view(int count)
        {
            int rounded = count % 10;
            if (count == 0)
                return "нет просмотров";
            else if (rounded == 1)
                return count.ToString() + " просмотр";
            else if (rounded > 1 && rounded <= 4)
                return count.ToString() + " просмотра";
            else
                return count.ToString() + " просмотров";
        }
        public string comment(int count)
        {
            int rounded = count % 10;
            if (count == 0)
                return "нет комментариев";
            else if (rounded == 1)
                return count.ToString() + " комментарий";
            else if (rounded > 1 && rounded <= 4)
                return count.ToString() + " комментария";
            else
                return count.ToString() + " комментариев";
        }
        public string item(int count)
        {
            int rounded = count % 10;
            if (count == 0)
                return "нет элементов";
            else if (rounded == 1)
                return count.ToString() + " элемент";
            else if (rounded > 1 && rounded <= 4)
                return count.ToString() + " элемента";
            else
                return count.ToString() + " элементов";
        }
    }
}

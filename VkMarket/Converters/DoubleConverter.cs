﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Windows.UI.Xaml.Data;

namespace VkMarket.Converters
{
    public class DoubleConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, 
            object parameter, string language)
        {
            double val = (double)value;
            string addition = parameter as string;
            if(!String.IsNullOrWhiteSpace(addition))
                return val + Double.Parse(addition);
            else
                return val;
        }

        // No need to implement converting back on a one-way binding 
        public object ConvertBack(object value, Type targetType, 
            object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}

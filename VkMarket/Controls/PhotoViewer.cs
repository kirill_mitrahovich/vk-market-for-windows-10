﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using VkMarket.Pages;
using VkMarket.Utils;

namespace VkMarket.Controls
{
    class PhotoViewer
    {
        private static readonly Object s_lock = new Object();
        private static PhotoViewer instance = null;

        private Popup m_popup;
        private FlipView m_flipView;
        private PhotoViewer()
        {
            SplitViewPage splitView = Window.Current.Content as SplitViewPage;
            if (splitView == null)
                return;

            splitView.SizeChanged += SplitView_SizeChanged;
            m_popup = splitView.FindName("PhotoViewerPopup") as Popup;
            m_flipView = splitView.FindName("PhotoFlipView") as FlipView;
        }

        private void SplitView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (m_popup != null)
            {
                m_popup.Width = Windows.UI.Xaml.Window.Current.Bounds.Width;
                m_popup.Height = Windows.UI.Xaml.Window.Current.Bounds.Height;
            }
        }

        public static PhotoViewer Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                Monitor.Enter(s_lock);
                PhotoViewer temp = new PhotoViewer();
                Interlocked.Exchange(ref instance, temp);
                Monitor.Exit(s_lock);
                return instance;
            }
        }
        public void Show(Object items, int index)
        {
            if (m_popup == null && m_flipView == null)
                return;
            m_flipView.ItemsSource = items;
            m_flipView.SelectedIndex = index;
            m_popup.IsOpen = true;
        }
    }

}

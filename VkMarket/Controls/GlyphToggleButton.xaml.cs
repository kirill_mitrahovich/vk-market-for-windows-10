﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace VkMarket.Controls
{
    public sealed partial class GlyphToggleButton : UserControl
    {
        public static DependencyProperty ToggledProperty = DependencyProperty.Register("Toggled",
            typeof(bool),
            typeof(GlyphToggleButton),
            new PropertyMetadata(true));
        public bool Toggled
        {
            get { return (bool)GetValue(ToggledProperty); }
            set { SetValue(ToggledProperty, value);}
        }


        public static DependencyProperty CheckedProperty = DependencyProperty.Register("Checked",
           typeof(int),
           typeof(GlyphToggleButton),
           new PropertyMetadata(0, new PropertyChangedCallback(CheckedChange)));
        public int Checked
        {
            get { return (int)GetValue(CheckedProperty); }
            set
            {
                var curr = (int)GetValue(CheckedProperty) != 0;
                if (curr != (value != 0) && Toggled)
                {
                    SetValue(CheckedProperty, value);
                    if (value != 0)
                        VisualStateManager.GoToState(this, "CheckedState", false);
                    else
                        VisualStateManager.GoToState(this, "Normal", false);
                }

            }
        }
        private static void CheckedChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as GlyphToggleButton;
            if (control.Toggled &&  (int)e.NewValue != 0)
                VisualStateManager.GoToState(control, "CheckedState", false);
            else
                VisualStateManager.GoToState(control, "Normal", false);

            if (control.CheckedChanged != null)
                control.CheckedChanged(d, e);
        }

        public event EventHandler<DependencyPropertyChangedEventArgs> CheckedChanged;


        public static DependencyProperty TextProperty = DependencyProperty.Register("Text",
            typeof(String),
            typeof(GlyphToggleButton),
            new PropertyMetadata(""));
        public String Text
        {
            get { return (String)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static DependencyProperty TextVisibilityProperty = DependencyProperty.Register("TextVisibility",
            typeof(Visibility),
            typeof(GlyphToggleButton),
            new PropertyMetadata(Visibility.Visible));
        public Visibility TextVisibility
        {
            get { return (Visibility)GetValue(TextVisibilityProperty); }
            set { SetValue(TextVisibilityProperty, value); }
        }

        public static DependencyProperty GlyphProperty = DependencyProperty.Register("Glyph",
           typeof(String),
           typeof(GlyphToggleButton),
           new PropertyMetadata(""));
        public String Glyph
        {
            get { return (String)GetValue(GlyphProperty); }
            set { SetValue(GlyphProperty, value); }
        }

        public static DependencyProperty GlyphVisibilityProperty = DependencyProperty.Register("GlyphVisibility",
          typeof(Visibility),
          typeof(GlyphToggleButton),
          new PropertyMetadata(Visibility.Visible));
        public Visibility GlyphVisibility
        {
            get { return (Visibility)GetValue(GlyphVisibilityProperty); }
            set { SetValue(GlyphVisibilityProperty, value); }
        }

        public GlyphToggleButton()
        {
            this.InitializeComponent();
            Loaded += GlyphToggleButton_Loaded;
        }

        private void GlyphToggleButton_Loaded(object sender, RoutedEventArgs e)
        {
            if (Checked != 0)
                VisualStateManager.GoToState(this, "CheckedState", false);
            else
                VisualStateManager.GoToState(this, "Normal", false);
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(Toggled)
                Checked = Checked != 0 ? 1 : 0;
        }
    }
}

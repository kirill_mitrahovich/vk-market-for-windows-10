﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace VkMarket.Controls
{
    public sealed partial class PowerTextControl : UserControl
    {
        private static Regex RegExHttpLinks = new Regex(@"[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b([\/?][-a-zA-Z0-9@:;%_\+.~#?&//=]*)?", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public static DependencyProperty TextContentProperty = DependencyProperty.Register("TextContent", typeof(String), typeof(PowerTextControl),
                new PropertyMetadata("", new PropertyChangedCallback(TextContentChanged)));
        public String TextContent
        {
            get { return (String)GetValue(TextContentProperty); }
            set { SetValue(TextContentProperty, value); }
        }

        private static void TextContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as PowerTextControl;
            if (control == null)
                return;

            var content = e.NewValue as String;
            if (content == null)
                return;

            control.ContentRichTextBlock.Blocks.Clear();

            Paragraph p = makeRichTextFromContent(content);

            control.ContentRichTextBlock.Blocks.Add(p);

            control.UpdateLayout();

            control.ExtendedSize = new Size(control.ContentRichTextBlock.ActualWidth,
                control.ContentRichTextBlock.ActualHeight + control.LessButton.ActualHeight + control.MoreButton.ActualHeight);



            if (control.ContentRichTextBlock.ActualHeight < 150 || 
                (String.IsNullOrWhiteSpace(control.LessLabel) && String.IsNullOrWhiteSpace(control.MoreLabel)))
            {
                if (String.IsNullOrWhiteSpace(control.Title))
                    VisualStateManager.GoToState(control, "NormalWithoutTitleState", false);
                else
                    VisualStateManager.GoToState(control, "Normal", false);
                control.VisualState = PowerTextState.Normal;
            }
            else
            {
                if (String.IsNullOrWhiteSpace(control.Title))
                    VisualStateManager.GoToState(control, "ShortWithoutTitleState", false);
                else
                    VisualStateManager.GoToState(control, "ShortDesctriptionState", false);

                control.VisualState = PowerTextState.Short;
            }
        }

        private static Paragraph makeRichTextFromContent(String str)
        {
            Paragraph p = new Paragraph();

            var matches = RegExHttpLinks.Matches(str);

            for (int i = 0; i < matches.Count; ++i)
            {
                if (i == 0)
                {
                    if (matches[i].Index > 0)
                        p.Inlines.Add(new Run() { Text = str.Substring(0, matches[i].Index - 1) });

                    try
                    {
                        Uri uri;
                        if (matches[i].Value.StartsWith(@"http://") )
                            uri = new Uri(matches[i].Value, UriKind.Absolute);
                        else
                            uri = new Uri(@"http://" + matches[i].Value, UriKind.Absolute);
                        p.Inlines.Add(new InlineUIContainer()
                        {

                            Child = new HyperlinkButton()
                            {
                                NavigateUri = uri,
                                Content = matches[i].Value.Length > 55 ? matches[i].Value.Remove(55) + "..." : matches[i].Value,
                                Style = App.Current.Resources["MyHyperlinkButtonStyle"] as Style
                            }
                        });
                    }
                    catch( Exception exp)
                    {
                        System.Diagnostics.Debug.WriteLine(exp.Message);
                    }
                    

                    int start = matches[i].Index + matches[i].Length;
                    if (start < str.Length)
                    {
                        if (matches.Count > 1)
                            p.Inlines.Add(new Run() { Text = str.Substring(start, matches[i + 1].Index - start) });
                        else
                            p.Inlines.Add(new Run() { Text = str.Substring(start) });
                    }

                }
                else if (i == matches.Count - 1)
                {
                    try
                    {
                        Uri uri;
                        if (matches[i].Value.StartsWith(@"http://"))
                            uri = new Uri(matches[i].Value, UriKind.Absolute);
                        else
                            uri = new Uri(@"http://" + matches[i].Value, UriKind.Absolute);

                        p.Inlines.Add(new InlineUIContainer() { Child = new HyperlinkButton() {
                            NavigateUri = uri,
                            Content = matches[i].Value,
                            Style = App.Current.Resources["MyHyperlinkButtonStyle"] as Style
                        } });
                    }
                    catch(Exception exp)
                    {
                        System.Diagnostics.Debug.WriteLine(exp.Message);
                    }

                    int startIndex = matches[i].Index + matches[i].Length;
                    p.Inlines.Add(new Run() { Text = str.Substring(startIndex) });
                }
                else
                {
                    try
                    {
                        Uri uri;
                        if (matches[i].Value.StartsWith(@"http://"))
                            uri = new Uri(matches[i].Value, UriKind.Absolute);
                        else
                            uri = new Uri(@"http://" + matches[i].Value, UriKind.Absolute);

                        p.Inlines.Add(new InlineUIContainer()
                        {
                            Child = new HyperlinkButton()
                            {
                                NavigateUri = uri,
                                Content = matches[i].Value,
                                Style = App.Current.Resources["MyHyperlinkButtonStyle"] as Style

                            }
                        });
                    }
                    catch (Exception exp)
                    {
                        System.Diagnostics.Debug.WriteLine(exp.Message);
                    }
                    int startIndex = matches[i].Index + matches[i].Length;
                    int finishIndex = matches[i + 1].Index;
                    p.Inlines.Add(new Run() { Text = str.Substring(startIndex, finishIndex - startIndex) });
                }


            }

            if (matches.Count == 0)
                p.Inlines.Add(new Run() { Text = str });

            return p;
        }

        public static DependencyProperty ExtendedSizeProperty = DependencyProperty.Register("ExtendedSize",
                typeof(Size),
                typeof(PowerTextControl),
                new PropertyMetadata(new Size(Double.NaN, Double.NaN)));
        public Size ExtendedSize
        {
            get { return (Size)GetValue(ExtendedSizeProperty); }
            set { SetValue(ExtendedSizeProperty, value); }
        }

        public static DependencyProperty VisualStateProperty = DependencyProperty.Register("ExtendedSize",
               typeof(PowerTextState),
               typeof(PowerTextControl),
               new PropertyMetadata(PowerTextState.None, new PropertyChangedCallback(VisualStateChange)));
        public PowerTextState VisualState
        {
            get { return (PowerTextState)GetValue(VisualStateProperty); }
            set { SetValue(VisualStateProperty, value); }
        }
        private static void VisualStateChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as PowerTextControl;
            if (control.VisualStateChanged != null)
                control.VisualStateChanged(d, e);
        }

        public event EventHandler<DependencyPropertyChangedEventArgs> VisualStateChanged;

        public static DependencyProperty TitleProperty = DependencyProperty.Register("Title",
            typeof(String),
            typeof(PowerTextControl),
            new PropertyMetadata(""));
        public String Title
        {
            get { return (String)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static DependencyProperty MoreLabelTextProperty = DependencyProperty.Register("MoreLabel",
            typeof(String),
            typeof(PowerTextControl),
            new PropertyMetadata(""));
        public String MoreLabel
        {
            get { return (String)GetValue(MoreLabelTextProperty); }
            set { SetValue(MoreLabelTextProperty, value); }
        }

        public static DependencyProperty LessLabelTextProperty = DependencyProperty.Register("LessLabel",
            typeof(String),
            typeof(PowerTextControl),
            new PropertyMetadata(""));
        public String LessLabel
        {
            get { return (String)GetValue(LessLabelTextProperty); }
            set { SetValue(LessLabelTextProperty, value); }
        }

        public PowerTextControl()
        {
            this.InitializeComponent();
            Loaded += PowerTextControl_Loaded;
        }

        private void PowerTextControl_Loaded(object sender, RoutedEventArgs e)
        {
            //if (ContentRichTextBlock.ActualHeight < 150 ||
            //   (String.IsNullOrWhiteSpace(LessLabel) && String.IsNullOrWhiteSpace(MoreLabel)))
            //{
            //    if (String.IsNullOrWhiteSpace(Title))
            //        VisualStateManager.GoToState(this, "NormalWithoutTitleState", false);
            //    else
            //        VisualStateManager.GoToState(this, "Normal", false);
            //    this.VisualState = PowerTextState.Normal;
            //}
            //else
            //{
            //    if (String.IsNullOrWhiteSpace(this.Title))
            //        VisualStateManager.GoToState(this, "ShortWithoutTitleState", false);
            //    else
            //        VisualStateManager.GoToState(this, "ShortDesctriptionState", false);

            //    this.VisualState = PowerTextState.Short;
            //}
        }

        private void LessButton_Click(object sender, RoutedEventArgs e)
        {
            if(String.IsNullOrWhiteSpace(Title))
                VisualStateManager.GoToState(this, "ShortWithoutTitleState", false);
            else
                VisualStateManager.GoToState(this, "ShortDesctriptionState", false);
            VisualState = PowerTextState.Short;
        }

        private void MoreButton_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(Title))
                VisualStateManager.GoToState(this, "LongWithoutTitleState", false);
            else
                VisualStateManager.GoToState(this, "LongDesctriptionState", false);
            VisualState = PowerTextState.Long;
        }
    }

    public enum PowerTextState { None, Normal, Short, Long }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VK.WindowsPhone.SDK.Util;

namespace VK.WindowsPhone.SDK.API.Model
{
    public partial class VKComment
    {
        public long id { get; set; }

        public long from_id { get; set; }

        public long date { get; set; }

        private string _text { get; set; }
        public string text
        {
            get { return _text; }
            set { _text = (value ?? "").ForUI(); }
        }

        public long reply_to_user { get; set; }

        public long reply_to_comment { get; set; }

        public List<VKAttachment> attachments { get; set; }

        public List<VKPhoto> attached_photos
        {
            get
            {
                if (attachments == null)
                    return null;

                var photo_attachments = attachments.Where(attach => attach.type == "photo");

                if (photo_attachments.Count() == 0)
                    return null;

                var ret = new List<VKPhoto>();

                foreach (var att in photo_attachments)
                {
                    if (att.photo != null)
                        ret.Add(att.photo);
                }

                return ret;
            }
        }


        private static Regex RegExLink = new Regex(@"\[(.*\|.*)\]", RegexOptions.IgnoreCase);
        public string plain_text {
            get
            {
                if (String.IsNullOrWhiteSpace(text))
                    return null;

                string ret = "" + text;
                var matches = RegExLink.Matches(ret);

                while ( matches.Count > 0)
                {
                    var name = ret.Substring(matches[0].Index, matches[0].Length);
                    name = name.Split('|')[1];
                    name = name.Remove(name.Length - 2);

                    ret = ret.Remove(matches[0].Index, matches[0].Length);
                    ret = ret.Insert(matches[0].Index, name);

                    matches = RegExLink.Matches(ret);
                }
                return ret;
            }
        }

    }
    public class VKCommentsResponse
    {
        public int              count { set; get; }
        public List<VKComment>  items { set; get; }
        public List<VKUser>     profiles { set; get; }
        public List<VKGroup>    groups { set; get; }
    }

}
